# Docker DotNet Core with Selenium 

A DotNet Core docker image with Selenium (Chrome) support

I should have recorded the steps when I built and pushed the image to DockerHub in the first place,
but I think the commands were along the lines of:

```
sudo docker build -t tickett/dotnet.core.selenium:latest .
sudo docker push tickett/dotnet.core.selenium:latest
```

Usage example:

```yaml
stages:
  - build
  - test

build:
 stage: build
 image: tickett/dotnet.core.selenium:latest
 tags:
  - docker
 script:
  - cp $NUGET_CONFIG ./NuGet.Config
  - dotnet build

test:
 stage: test
 image: tickett/dotnet.core.selenium:latest
 tags:
  - docker
 script:
  - cp $NUGET_CONFIG ./NuGet.Config
  - dotnet build
  - nohup dotnet run --project Web --no-restore &
  - dotnet test -v=normal Tests/Tests.csproj /p:CollectCoverage=true --logger:"junit;LogFilePath=test-result.xml" --collect:"XPlat Code Coverage"
 coverage: '/Average\s*\|.*\|\s(\d+\.?\d*)%\s*\|.*\|/'
 artifacts:
  when: always
  reports:
   junit: ./Tests/test-result.xml
   cobertura: ./Tests/TestResults/**/coverage.cobertura.xml
```
