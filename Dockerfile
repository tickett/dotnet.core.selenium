FROM mcr.microsoft.com/dotnet/core/sdk:latest

ENV PATH="/tmp/sqlpackage:${PATH}"

RUN export CHROME_DRIVER_VERSION=$(curl -sS https://chromedriver.storage.googleapis.com/LATEST_RELEASE) \
&& wget -qO- https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip | gunzip > /tmp/chromedriver \
&& install -m 0755 -o root -g root /tmp/chromedriver /usr/local/bin \
&& wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
&& echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list \
&& apt-get update \
&& apt-get install -y google-chrome-stable unzip --no-install-recommends \
&& wget -O /tmp/sqlpackage.zip https://aka.ms/sqlpackage-linux \
&& unzip /tmp/sqlpackage.zip -d /tmp/sqlpackage \
&& rm /tmp/sqlpackage.zip \
&& chmod +x /tmp/sqlpackage/sqlpackage
